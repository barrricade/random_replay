import random
import pandas 
import sys
import os
from datetime import datetime
import time
# from traffic_detector.config import 
sys.path.append("./")
src_ip_df = pandas.read_csv('./src_ip/table.csv')
#src_ip_df = pandas.read_csv('random_replay/src_ip/thread.csv')
dst_ip_df = pandas.read_csv('./src_ip/table2.csv')
src_ip_list  = src_ip_df['message_info.dst_ip: Descending'].values
#src_ip_list = src_ip_df['content'].values
dst_ip_list = dst_ip_df['dst_ip'].values
while True:
    src_ip = random.choice(src_ip_list)
    dst_ip = random.choice(dst_ip_list)
    #随机选择一个pcap包
    # pcap = random.choice(os.listdir('traffic_detector/random_replay/pcapFile')) 
    #pcaps = random.choice(os.listdir('traffic_detector/random_replay/pcapFile')) 
    pcaps = os.listdir('random_replay/pcapFile')
    pcaps = random.sample(pcaps, 10)
    for p in pcaps:
        #pcap = p
        pcap =  p
        print(pcap)
        #分割pcap包
        # os.system(f'tcpdump -r traffic_detector/random_replay/pcapFile/{pcap} -c 50 -w traffic_detector/random_replay/pcapFile/output.pcap')
        #重写目标ip
        os.system(f'tcprewrite --infile=random_replay/pcapFile/{pcap} --outfile=random_replay/temp/resetdst.pcap --dstipmap=0.0.0.0/0:{dst_ip} --enet-dmac=74:d4:35:88:68:e6')
        #重写源ip地址
        os.system(f'tcprewrite --infile=random_replay/temp/resetdst.pcap --outfile=random_replay/temp/resetsrc.pcap --dstipmap=0.0.0.0/0:{src_ip} --enet-dmac=74:d4:35:88:68:e6')
        #数据包校验
        os.system('tcprewrite --infile=random_replay/temp/resetsrc.pcap --outfile=random_replay/temp/rsyslogfinal.pcap --fixcsum')
        #重放流量
        os.system('tcpreplay -v -i enp4s1 --maxsleep 1 random_replay/temp/rsyslogfinal.pcap')
        print(f"execute reply++++++{datetime.now()}",src_ip,pcap)
    time.sleep(2)
